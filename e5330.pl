#!/usr/bin/perl
# Author: Ekin Karadeniz (c) 2018

use strict;
use warnings;
use LWP::UserAgent;
use XML::Simple;
use MIME::Base64;
use Data::Dumper;
use Date::Format;

my $hostname = "192.168.0.1";
my $password = "admin";
my $api = "http://$hostname/api";

my $ua = LWP::UserAgent->new;
$ua->default_headers->header("Referer", $api);
$ua->agent("iamdual/e5330.pl");

sub parse {
    my $xs = XML::Simple->new;
    if ($_[1]) {
        return $xs->XMLin($_[0], ForceArray => 1); 
    } else {
        return $xs->XMLin($_[0]);
    }
}

sub get_token {
    my $req = HTTP::Request->new(GET => "$api/webserver/token");
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml->{token};
    }
    return;
}

sub login {
    my $encoded = encode_base64($password, "");
    my $req = HTTP::Request->new(POST => "$api/user/login");
    $req->header(":__RequestVerificationToken" => get_token());
    $req->content_type("application/xml");
    $req->content("<?xml version: '1.0' encoding='UTF-8'?><request><Username>admin</Username><Password>$encoded</Password></request>");
    $ua->request($req);
    return;
}

# 0: box type (1=inbox, 2=sent)
sub sms_list {
    my $req = HTTP::Request->new(POST => "$api/sms/sms-list");
    $req->header(":__RequestVerificationToken" => get_token());
    $req->content_type("application/xml");
    $req->content("<?xml version: '1.0' encoding='UTF-8'?><request><PageIndex>1</PageIndex><ReadCount>50</ReadCount><BoxType>$_[0]</BoxType><SortType>0</SortType><Ascending>1</Ascending><UnreadPreferred>0</UnreadPreferred></request>");
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content, 1);
        return $xml->{Messages}[0]->{Message};
    }
    return;
}

sub sms_stats {
    my $req = HTTP::Request->new(GET => "$api/sms/sms-count");
    $req->header(":__RequestVerificationToken" => get_token());
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml;
    }
    return;
}

# 0: phone number
# 1: content
sub sms_send {
    my $date = time2str("%Y-%m-%d %H:%M:%S", time);
    my $req = HTTP::Request->new(POST => "$api/sms/send-sms");
    $req->header(":__RequestVerificationToken" => get_token());
    $req->content_type("application/xml");
    $req->content("<?xml version: '1.0' encoding='UTF-8'?><request><Index>-1</Index><Phones><Phone>$_[0]</Phone></Phones><Sca></Sca><Content>$_[1]</Content><Length>18</Length><Reserved>1</Reserved><Date>$date</Date></request>");
    $ua->request($req);
    return;
}

# 0: item index
sub sms_delete {
    my $req = HTTP::Request->new(POST => "$api/sms/delete-sms");
    $req->header(":__RequestVerificationToken" => get_token());
    $req->content_type("application/xml");
    $req->content("<?xml version: '1.0' encoding='UTF-8'?><request><Index>$_[0]</Index></request>");
    $ua->request($req);
    return;
}

# 0: item type (1=inbox, 2=sent)
sub sms_purge {
    my $sms_list = sms_list($_[0]);
    if ($sms_list) {
        foreach my $sms (@{$sms_list}) {
            sms_delete($sms->{Index}[0]);
        }
    } else {
        print "No SMS found.";
    }
}

sub network_stats {
    my $req = HTTP::Request->new(GET => "$api/monitoring/traffic-statistics");
    $req->header(":__RequestVerificationToken" => get_token());
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml;
    }
    return;
}

sub network_status {
    my $req = HTTP::Request->new(GET => "$api/monitoring/status");
    $req->header(":__RequestVerificationToken" => get_token());
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml;
    }
    return;
}

sub device_info {
    my $req = HTTP::Request->new(GET => "$api/device/information");
    $req->header(":__RequestVerificationToken" => get_token());
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml;
    }
    return;
}

sub wifi_hosts {
    my $req = HTTP::Request->new(GET => "$api/wlan/host-list");
    $req->header(":__RequestVerificationToken" => get_token());
    my $res = $ua->request($req);
    if ($res->is_success) {
        my $xml = parse($res->content);
        return $xml;
    }
    return;
}
